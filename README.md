# 远程控制

# 1.需求分析

## 1.1 文件需求

- 观察文件
- 打开文件
- 下载文件
- 删除文件

## 1.2 观察需求

- 远程监控

## 1.3 操作需求

- 鼠标操作
- 锁机/解锁

# 2.技术分析

**网络编程项目**

## 2.1 服务端(受控端)

- 网络编程
- 文件处理
- 鼠标处理
- 图像处理

## 2.2 客户端(控制端)

- 网络编程
- MFC编程
- 图像处理

# 3. 环境配置

## 3.1 MFC的安装教程

[【Microsoft Visual Studio】安装教程超详解-CSDN博客](https://blog.csdn.net/weixin_72959097/article/details/132308653)

[VS2019以及MFC的安装（详细）_visual studiomfc在哪安装-CSDN博客](https://blog.csdn.net/Mrweng1996/article/details/103202297?ops_request_misc=&request_id=&biz_id=102&utm_term=MFC%E5%BC%80%E5%8F%91Visual%20Studio%E5%AE%89%E8%A3%85%E6%95%99%E7%A8%8B&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-0-103202297.142^v100^pc_search_result_base5&spm=1018.2226.3001.4187)

## 3.2代码提交忽略项

`.vs`是代码分析的临时文件，没有重用性，没有必要提交

`.user` 因人而异，需要忽略

> 注意：
`.filters` 项目文件配置
`.vcxproj` 项目文件

## 3.3 Visual Studio 中文注释乱码

[关于git推送代码到github远程仓库中文乱码问题，visual studio保存文件默认编码格式问题_git不能显示rg2312的中文-CSDN博客](https://blog.csdn.net/Motarookie/article/details/134101643?ops_request_misc=&request_id=&biz_id=102&utm_term=Visual%20Studio%E4%B8%8A%E5%86%99%E4%B8%AD%E6%96%87%E6%B3%A8%E9%87%8A%E6%8F%90%E4%BA%A4%E4%BB%93%E5%BA%93%E6%98%BE%E7%A4%BA%E4%B9%B1%E7%A0%81&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-4-134101643.142^v100^pc_search_result_base1&spm=1018.2226.3001.4187)

## 3.4 多字节字符集和Unicode字符集的区别

[C++ 多字节编码与Unicode编码_c++ unicode-CSDN博客](https://blog.csdn.net/Zander0/article/details/116452035?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522171293710616800184199366%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=171293710616800184199366&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-1-116452035-null-null.142^v100^pc_search_result_base9&utm_term=%E5%A4%9A%E5%AD%97%E8%8A%82%E5%AD%97%E7%AC%A6%E9%9B%86%E5%92%8CUnicode%E5%AD%97%E7%AC%A6%E9%9B%86&spm=1018.2226.3001.4187)

多字节字符集（MBCS，Multibyte Character Set）和 Unicode 字符集是两种不同的字符编码方式，它们之间存在着一些区别：

- **编码范围：**
    - MBCS 是一种可变长度的字符编码方式，它可以表示多种不同的字符集，包括 ASCII、ISO-8859 等单字节字符集，以及各种双字节字符集（如 GBK、Shift-JIS 等）。MBCS 使用单字节或多字节来表示不同的字符，不同的字符集有不同的编码范围。
    - Unicode 是一种字符集标准，它统一了世界上几乎所有的字符，并为每个字符分配了唯一的码点。Unicode 的编码范围非常广泛，涵盖了几乎所有的语言、符号和表情。
- **编码长度：**
    - MBCS 中的字符可以是单字节字符，也可以是多字节字符。单字节字符只需要一个字节来表示，而多字节字符则需要两个或更多字节来表示，具体长度取决于字符集和具体的字符。
    - Unicode 中的字符都是固定长度的，UTF-8 编码下每个字符使用 1 到 4 个字节表示，UTF-16 编码下每个字符使用 2 个字节表示，UTF-32 编码下每个字符使用 4 个字节表示。
- **兼容性：**
    - MBCS 是一种早期的字符编码方式，在早期的操作系统和应用程序中被广泛使用。它可以很好地兼容单字节字符集，并且在处理 ASCII 字符时具有很高的效率。
    - Unicode 是一种现代的字符编码方式，它解决了 MBCS 中存在的字符集混乱、编码不一致等问题。Unicode 的优势在于统一了世界上的字符，使得不同语言、不同平台之间的文本处理更加简单和统一。

总的来说，MBCS 是一种早期的字符编码方式，适用于特定的地区和语言环境；而 Unicode 是一种现代的字符编码方式，具有更广泛的字符范围和更好的兼容性，是现代软件开发中的主流字符编码方式。

## 3.4 将服务端变成完全的后台进程

![Untitled](README_image/Untitled.png)

![Untitled](README_image/Untitled%201.png)

![Untitled](README_image/Untitled%202.png)

可以根据上述图片去设置子系统和程序入口，也可以在`main.cpp`中添加预编译命令：

```cpp
#pragma comment( linker, "/subsystem:windows /entry:WinMainCRTStartup" )
// 远程控制用到window子系统和mainCRTStartup
#pragma comment( linker, "/subsystem:windows /entry:mainCRTStartup" )
#pragma comment( linker, "/subsystem:console /entry:mainCRTStartup" )
#pragma comment( linker, "/subsystem:console /entry:WinMainCRTStartup" )
```

当使用 `#pragma comment` 指令来设置程序的子系统和入口点时，通常会根据应用的需求选择合适的设置。下面是这四种设置的典型应用场景：

1. `#pragma comment( linker, "/subsystem:windows /entry:WinMainCRTStartup" )`：
    - **典型应用场景**：Windows GUI 应用程序
    - **说明**：这种设置适用于那些使用 Windows 图形用户界面 (GUI) 的应用程序，通常使用 `WinMain` 函数作为程序的入口点。这样的程序在启动时不会打开控制台窗口，而是直接打开 GUI 界面。常见的例子包括图形编辑器、游戏等需要交互式界面的应用程序。
2. `#pragma comment( linker, "/subsystem:windows /entry:mainCRTStartup" )`：
    - **典型应用场景**：特殊的 Windows 应用程序
    - **说明**：这种设置同样用于 Windows 应用程序，但使用 `main` 函数作为入口点。这种设置可能用于某些特殊情况，例如手动编写程序入口的情况，或者在程序中需要特殊的初始化操作。
3. `#pragma comment( linker, "/subsystem:console /entry:mainCRTStartup" )`：
    - **典型应用场景**：控制台应用程序
    - **说明**：这种设置适用于那些在控制台中执行的应用程序，通常使用 `main` 函数作为入口点。这样的程序在启动时会打开一个控制台窗口，可以在其中进行命令行交互。典型的控制台应用程序包括命令行工具、批处理脚本等。
4. `#pragma comment( linker, "/subsystem:console /entry:WinMainCRTStartup" )`：
    - **典型应用场景**：控制台应用程序模拟 GUI 行为
    - **说明**：这种设置用于将控制台应用程序模拟成 Windows GUI 应用程序的行为。它指定使用 `WinMain` 函数作为入口点，但程序仍然会以控制台模式运行。这种设置可能在需要在控制台环境中模拟 GUI 行为的特殊情况下使用，但不常见。

根据应用的需求和所处的开发环境，选择适合的设置是很重要的，以确保程序能够在目标环境中正确运行并达到预期的效果。

# 服务端

# 4. 网络编程(实现`socket`类单例模式)

## 用于网络层通信，连接客户端、接收发数据

服务端网络编程套路：`socket,bind,listen,accept,send,recv,close` 

# 5. Packet数据包类

## 设计的数据协议，组包和拆包

## 5.1 拆包：借助构造函数实现

- 缺包：直接退出拆包过程
- 粘包：解包完成（不论是否成功解包），将传入的总包长`修改成已经完成解包的包长` ，再将未完成解包的部分挪到buffer的首位，如果又收到包向后补充buffer即可
- 最后注意校验包

## 5.2 组包：借助构造函数实现，无难度

发送：`return send(m_clientSocket, (const char*)&pack, pack.Size(), 0);`

### 5.2.1 组包的包数据错误

```cpp
WORD sHead;          // 固定位FE FF 2字节
DWORD nLength;       // 包长度 从控制命令到和校验结束 4
WORD sCmd;           // 控制命令 2
std::string strData; // 包数据
WORD sSum;           // 和校验 2
```

组包调试时，strData部分为`C,D` ,包的16进制：`FF FE CC CC 07 00 00 00 01 00 CC CC 50` 

错误原因如下：

- **类的对象内存对齐问题：需要将对齐方式设置成1字节对齐**

```cpp
#pragma pack(push, 1) // 保存当前对齐设置，并设置对齐值为 1 字节

struct MyStruct {
    char a; // 占用 1 字节
    int b;  // 占用 4 字节
    char c; // 占用 1 字节
};

#pragma pack(pop) // 恢复之前的对齐设置
```

更改后，组包内容为：`FF FE 07 00 00 00 01 00 38 99 A4 00 43`

- **使用`&pack` 去传入包数据，`std::string` 对象得不到内容，得到的是地址**

解决如下：

封装一个方法，拿出string对象的数据。

```cpp
const char* Packet::Data()
{
	strOut.resize(Size());
	BYTE* pData = (BYTE*)strOut.c_str();
	*(WORD*)pData = sHead; pData += sizeof(sHead);
	*(DWORD*)(pData) = nLength; pData += sizeof(nLength);
	*(WORD*)pData = sCmd; pData += sizeof(sCmd);
	// 拿出strData，拷贝到strOut上
	memcpy(pData, strData.c_str(), strData.size()); pData += strData.size();
	*(WORD*)pData = sSum;

	return strOut.c_str();
}
```

`send(m_clientSocket, pack.Data(), pack.Size(), 0);`

# 6.文件需求

## 6.1 概述

- 观察文件
- 打开文件
- 下载文件
- 删除文件

## 6.2 查看磁盘分区

使用`_chdrive()`函数去获取计算机磁盘分区，并用`send`去发送给客户端

## 6.1 与文件相关库函数

### 6.1.1 `_chdrive()`函数

`_chdrive()` 函数是用于**改变当前默认驱动器的函数**，通常用于在 DOS 或 Windows 命令行环境中。它是一个非标准的函数，属于 Microsoft 的 C 运行时库中的一部分。它的功能是改变当前的默认驱动器。

函数原型如下：

```cpp
int _chdrive(int drive);

```

参数 `drive` 是要设置为当前驱动器的逻辑驱动器号，通常是一个在 ASCII 字母 `'A'` 到 `'Z'` 之间的值，分别代表不同的逻辑驱动器。

这个函数返回值为 `0` 表示成功，非 `0` 表示失败。失败可能是由于指定的驱动器号不存在或其他原因。

以下是一个简单的示例：

```cpp
#include <stdio.h>
#include <dos.h>

int main() {
    int result = _chdrive(3); // 将当前驱动器改变为驱动器 D
    if (result == 0) {
        printf("Changed drive successfully.\\n");
    } else {
        printf("Failed to change drive.\\n");
    }
    return 0;
}

```

这个程序尝试将当前驱动器更改为驱动器 D（`'D'`），并根据返回值输出相应的消息。

**在本次开发中，用该函数判断当前window环境有多少个磁盘**

## 6.2访问文件名（观察文件）

### 主要是业务逻辑的书写

以下是一个使用 `_chdir`、`_findfirst` 和 `_findnext` 函数来访问某个文件的简单示例：

```cpp
#include <iostream>
#include <io.h>
#include <string>

int main() {
    // 切换到指定目录
    if (_chdir("C:\\\\path\\\\to\\\\directory") != 0) {
        std::cerr << "Failed to change directory!" << std::endl;
        return 1;
    }

    // 查找文件
    intptr_t handle;
    struct _finddata_t fileinfo;

    // 在指定目录下查找第一个文件
    handle = _findfirst("*.*", &fileinfo);
    if (handle == -1) {
        std::cerr << "No files found!" << std::endl;
        return 1;
    }

    // 输出第一个文件信息
    std::cout << "First file found: " << fileinfo.name << std::endl;

    // 继续查找下一个文件
    while (_findnext(handle, &fileinfo) == 0) {
        std::cout << "Next file found: " << fileinfo.name << std::endl;
    }

    // 关闭文件查找句柄
    _findclose(handle);

    return 0;
}

```

在这个示例中：

- 使用 `_chdir` 函数将当前工作目录切换到指定的目录。
- 使用 `_findfirst` 函数在指定目录下查找第一个文件，并返回一个文件查找句柄。
- 使用 `_findnext` 函数循环遍历查找下一个文件，直到所有文件都被找到。
- 使用 `_findclose` 函数关闭文件查找句柄。

请注意，你需要将 `"C:\\\\path\\\\to\\\\directory"` 替换为实际的目录路径。此外，`*.*` 是通配符，表示查找所有文件。

## 6.3 打开（运行）文件

`ShellExecuteA(NULL, NULL, strPath.c_str(), NULL, NULL, SW_SHOWNORMAL);`

`ShellExecuteA` 是一个 Windows API 函数，用于**执行外部程序或者打开文件、文件夹、网址等**。它的函数原型如下：

```cpp
HINSTANCE ShellExecuteA(
  HWND    hwnd,
  LPCSTR  lpOperation,
  LPCSTR  lpFile,
  LPCSTR  lpParameters,
  LPCSTR  lpDirectory,
  INT     nShowCmd
);
```

参数说明如下：

- `hwnd`: 指定父窗口的句柄，如果不需要父窗口则可以设为 `NULL`。
- `lpOperation`: 指定操作类型，常见的操作类型包括 `"open"`、`"edit"`、`"print"` 等，如果不需要指定操作类型则可以设为 `NULL`。
- `lpFile`: 指定要执行的文件、文件夹或者网址的路径或者名称。
- `lpParameters`: 指定传递给要执行的程序的参数，如果不需要参数则可以设为 `NULL`。
- `lpDirectory`: 指定工作目录，如果不需要指定工作目录则可以设为 `NULL`。
- `nShowCmd`: 指定程序窗口的显示方式，常见的值包括 `SW_SHOWNORMAL`、`SW_HIDE`、`SW_SHOWMAXIMIZED` 等。

`ShellExecuteA` 函数执行成功时返回一个大于 32 的值，代表成功执行的程序实例的句柄。如果执行失败，则返回一个对应的错误码。

使用 `ShellExecuteA` 函数可以方便地打开文件、文件夹、网址等，而且它会自动调用关联程序打开相应的文件类型。这使得在 Windows 环境下执行外部程序或者打开文件变得非常简单和方便。

## 6.4 下载文件

首先告知文件大小，再每1024字节传输文件。

### 6.4.1 `fopen_s` 函数

`fopen_s` 是 C 标准库中用于打开文件的函数，其原型通常为：

```c
errno_t fopen_s(FILE** pFile, const char* filename, const char* mode);

```

`fopen_s` 函数用于打开一个文件，并返回一个指向文件的指针。与传统的 `fopen` 函数不同，`fopen_s` 是 C11 标准中新增的函数，旨在提供更安全的文件操作。它接受三个参数：

- `pFile`: 一个指向 `FILE*` 类型指针的指针。如果打开文件成功，则 `pFile` 将指向打开的文件流；如果打开文件失败，则 `pFile` 会被置为 `NULL`。
- `filename`: 要打开的文件的名称，以字符串形式表示。
- `mode`: 一个表示打开文件的模式的字符串，包括只读、读写、追加等。这个参数的含义和 `fopen` 函数中的模式参数相同。

`fopen_s` 的返回值是一个错误码，如果函数执行成功，返回值为零；如果函数执行失败，返回一个对应的错误码，用于指示具体的错误原因。通常情况下，如果函数执行成功，则 `pFile` 指向打开的文件流；如果函数执行失败，则 `pFile` 被置为 `NULL`，并且返回一个错误码来指示错误原因。

使用 `fopen_s` 函数可以避免传统的 `fopen` 函数中的一些安全问题，比如打开文件时可能会覆盖掉已经打开的文件，或者在非法路径下打开文件等。因此，在 C11 标准及之后的版本中，建议优先使用 `fopen_s` 函数进行文件操作，以提高程序的安全性。

> **`"rb"`**：二进制只读模式。类似于 **`"r"`** 模式，但用于打开二进制文件。
> 

### 6.4.2 获取文件大小

- 文件指针偏移到末尾
- 获取文件指针偏移量
- 恢复文件指针偏移

### 6.4.3 报错：1>LINK : fatal error LNK1168: 无法打开 D:\WorkFile\CodeWareHouse\remote-control\Debug\RemoteCrl.exe 进行写入

那是因为编译的exe文件正在运行，而再次编译是要修改这个exe文件的，因为不让写，所以就抱着这个错。

解决：打开任务管理器，找到`***.exe` 把这个进程关掉就可以了。

# 7. 操作需求

## 7.1 鼠标操作需求

鼠标操作需要知道三要素：**鼠标坐标、按键类型、对按键执行的动作**：

1. **鼠标坐标**：表示鼠标在屏幕上的位置，通常用屏幕上的像素坐标来表示。鼠标坐标是一个二维坐标系，以屏幕左上角为原点，向右为 x 轴正方向，向下为 y 轴正方向。
2. **按键类型**：表示鼠标按下或释放的按键类型，通常包括左键、右键和中键。不同的操作系统和程序可能还支持更多的按键类型，如扩展键、侧键等。
3. **对按键执行的动作**：表示在特定鼠标事件（如按下、释放、移动等）发生时，系统或程序执行的动作。例如，在鼠标左键按下时执行的操作可以是拖拽、选中、点击等。

除此之外，还有一些其他的要素可能会影响鼠标操作，例如：

- **鼠标滚轮操作**：表示鼠标滚轮的滚动方向和滚动的距离，通常用来进行页面滚动、缩放等操作。
- **鼠标双击操作**：表示连续两次点击鼠标左键的时间间隔和动作，通常用来执行一些特定的快捷操作。
- **鼠标移动速度**：表示鼠标在屏幕上移动的速度，可能会影响到一些特定的操作和游戏体验。

综上所述，对于鼠标操作，除了鼠标坐标、按键类型和对按键执行的动作外，还需要考虑到一些其他的要素，以实现更丰富和精确的用户交互。

### 7.1.1 Windows的Api

在 Windows 平台下，实现鼠标操作的相关 API 函数主要包括以下几个：

- **GetCursorPos**：用于获取鼠标当前位置的屏幕坐标。
- **SetCursorPos**：用于设置鼠标的屏幕坐标位置。
- **GetAsyncKeyState**：用于检查鼠标按键的状态，例如左键、右键和中键的按下状态。
- **mouse_event**：用于模拟鼠标事件，如移动、点击、释放等。
- **SendInput**：用于发送输入事件，可以模拟鼠标和键盘事件。
- **RegisterRawInputDevices**：用于注册原始输入设备，可以获取鼠标的原始输入数据。
- **GetRawInputData**：用于获取原始输入设备的数据，包括鼠标的移动和按键事件等。

这些函数提供了一系列操作鼠标的功能，可以实现对鼠标坐标、按键状态和事件的监测、模拟和控制。使用这些函数可以编写 Windows 应用程序或者游戏，实现对鼠标操作的各种功能和交互效果。

### 7.1.2 鼠标操作开发

本次开发服务端用到的函数为**`SetCursorPos`** 和**`mouse_event`** ，对于按键类型、对按键执行的动作，完成组合，再用Switch去分类即可。

[SetCursorPos 函数 (winuser.h) - Win32 apps | Microsoft Learn](https://learn.microsoft.com/zh-cn/windows/win32/api/winuser/nf-winuser-setcursorpos)

[mouse_event 函数 (winuser.h) - Win32 apps | Microsoft Learn](https://learn.microsoft.com/zh-cn/windows/win32/api/winuser/nf-winuser-mouse_event)

# 8. 远程监控

## 8.1 步骤

```cpp
    /**
     * 1、将屏幕图像复制到screen对象中
     * 2、在全局内存块中创建数据流对象
     * 3、将screen 对象中的图像保存到数据流对象中
     * 4、将全局内存块数据存入packet并发送
     * 5、释放数据流对象，全局内存块，设备上下文
    */
```

大致是使用screen 对象去保存当前屏幕，再生成一个数据流去临时存储并发送出去。

## 8.2屏幕相关基础概念

### 8.2.1 屏幕设备的上下文（Device Context）

Windows 系统中的一个概念，它是用来**描述和管理屏幕或者其他图形设备的状态和属性的数据结构**。每个窗口、位图、图标等图形对象都有一个与之关联的设备上下文。

屏幕设备的上下文包含了一系列的属性和状态信息，用于描述屏幕的图像属性、绘图参数、颜色模式、剪切区域、字体等。通过屏幕设备上下文，程序可以与屏幕进行交互，实现绘图、渲染、复制、移动等操作。

# 9. 锁机与解锁

## 9.1 创造对话框模板资源去作为弹窗去覆盖服务端主机

- 更改窗口ID，取消系统菜单，样式设置为overlapped，取消边框
- 创造对应.h和.cpp文件
- 注：可以选择遮蔽整个屏幕或者是局部遮蔽（目前选择局部遮蔽）

## 9.2 dialog的使用流程

- **对话框详解(模态与非模态)：详细见`mfc基础课.pdf` 搜索 对话框**
- 对话框依赖于消息循环，若没有消息循环，对话框启动不了

```cpp
    // 1.创造非模态对话框 绑定对话框模板资源id
    dlg.Create(IDD_DIALOG_INFO, NULL);
    // 2.将窗口显示在屏幕上并激活
    dlg.ShowWindow(SW_SHOW);
    // 3.将 dlg 对话框对象设置为顶层窗口
    dlg.SetWindowPos(&dlg.wndTopMost, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);

    // 4.消息循环
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0)){
    ........
    ........
    }
```

## 9.3 限制鼠标

### 9.3.1 限制鼠标在弹窗范围内移动

```cpp
    // 限制鼠标移动范围在dlg内
    CRect rect;
    dlg.GetWindowRect(rect);
    ClipCursor(rect);
```

### 9.3.2 将鼠标设置为不可见

```cpp
    // 设置鼠标不可见
    ShowCursor(false);
    
    // 恢复鼠标为可见
    ShowCursor(true);
```

## 9.3 创建线程去执行锁机

**创建线程函数去执行dialog和鼠标限制等，避免主线程阻塞。**

使用线程的需要注意：创建线程，清理线程资源

会使用函数`_beginthreadex` ，`_endthreadex` 即可。

## 9.4 解锁

向线程发送模拟按下`Esc`消息：**`postThreadMessageW`**

# 客户端

# 10.网络编程

服务端网络编程套路：`socket,connect,write,read,close` 

复制粘贴服务端的socket即可，修改内容如下：

`InitSocket`

## 10.1 服务端与客户端联调

VS解决方案右键设置启动项

# 11.MFC编程

## 11.1 添加ip地址和端口控件

添加两个控件，并把控件里的ip和端口同步到代码

## 11.2 文件树功能

- 发送磁盘分区请求，调用接收数据接口(阻塞)，解析数据并更新到控件

## 11.2.1 展开树触发事件，以更新磁盘内文件内容

![Untitled](README_image/Untitled%203.png)

## 11.3 List Control展示文件信息

依旧是双击触发。添加List Control的变量，将文件(非目录)信息更新到list上去。

![Untitled](README_image/Untitled%204.png)

## 11.4 下载功能

- 添加文件的右键菜单栏。添加右键的事件回调，回调里执行下载功能。
- 添加菜单中下载文件的处理函数

### **11.4.1 下载需要优化，遇到大文件传输，会阻塞主线程，无法与用户进行交互**

解决方法：开线程去执行下载任务+窗口展示

![Untitled](README_image/Untitled%205.png)

### 11.4.2 `PostThreadMessage`线程间通信

`PostThreadMessage` 是一个 Windows 平台上的函数，用于向指定线程的消息队列中发送消息。它的原型如下：

```cpp
BOOL PostThreadMessage(
  DWORD  idThread,
  UINT   Msg,
  WPARAM wParam,
  LPARAM lParam
);

```

其中：

- `idThread`：目标线程的线程标识符，通常是线程的ID。
- `Msg`：要发送的消息类型。
- `wParam` 和 `lParam`：消息的附加参数，其意义根据消息类型而定。

`PostThreadMessage` 的作用是将消息发送到指定线程的消息队列中，并立即返回，不等待消息被处理。这意味着，即使目标线程当前正在处理其他消息或者处于等待状态，`PostThreadMessage` 也会立即返回。这与 `SendMessage` 不同，后者会等待消息被目标窗口处理完毕才会返回。

使用 `PostThreadMessage` 可以实现线程间的通信，例如，你可以在一个线程中通过 `PostThreadMessage` 发送消息给另一个线程，从而实现线程之间的数据传递或者触发特定操作。通常情况下，这种方式比共享内存或者全局变量更安全，因为它避免了线程间的竞争条件和同步问题。

需要注意的是，`PostThreadMessage` 仅适用于在**同一个进程内的不同线程之间发送消息**。如果你需要在不同进程之间通信，则需要使用其他机制，如消息队列、管道、共享内存等。

### 1.4.3 副线程与类内主线程通信

[下载任务线程去发送消息给主线程执行SendCommandPacket · 17b90a9 · YinaYan/RemoteControl - Gitee.com](https://gitee.com/YinaYan/remote-control/commit/17b90a94b2d0b19b3c0af009fcddb6b0b3408682)

**注意：在主线程中函数可以执行，却不能在线程内执行，问题大概率是某些资源只有主线程才可以用，只要把出问题的函数放在主线程执行，即副线程发送消息让主线程去执行即可。**

# 12.图像处理

## 12.1 远程监控显示

1.数据处理：单开线程，需要缓存区暂存图像数据，如果客户端取慢了，服务端需要丢旧帧保证实时性。

2.显示图片：新建对话框，为该对话框添加定时器，并初始化激活定时器，定时器实现远程桌面显示

![Untitled](README_image/Untitled%206.png)

3.调整缩放比例

- 调整图像为整体显示
- 将窗口的宽高比(8:5)调整至图像的宽高比，确保图像正常显示

![Untitled](README_image/Untitled%207.png)

## 12.2 鼠标控制

- **获取客户端鼠标操作**

可以从对话框类属性的消息里添加

**将客户端操作打包成数据并发送给服务端**

**服务端解析数据，并执行相应操作**

# 项目优化

# 13. UML和时序图的绘制

# 14.用映射表代替Switch case

## 14.1 使用映射表代替 `switch` 语句在以下场合可能更好：

1. **可扩展性：** 当需要处理的命令或条件很多时，使用 `switch` 语句会导致代码冗长，而使用映射表可以更容易地添加、删除或修改条件，使得代码更具扩展性。
2. **可读性：** 使用映射表可以使代码更具可读性，因为映射表明确地列出了每个条件与其对应的操作，而不需要深入理解 `switch` 语句的控制流。
3. **减少重复：** 如果某些条件具有相同的操作，使用映射表可以减少重复代码，提高代码的可维护性。
4. **灵活性：** 映射表可以是动态的，因此可以在运行时根据需要动态修改映射关系，而 `switch` 语句在编译时已经确定了条件与操作的对应关系。
5. **可测试性：** 使用映射表可以更容易地进行单元测试，因为每个条件的处理可以单独地进行测试，而不需要考虑整个 `switch` 语句的控制流。

总的来说，使用映射表可以提高代码的可读性、可维护性和灵活性，特别是在需要处理多个条件或操作时，映射表通常更适合于这样的情况。

本项目中，由于switch最多256个，且越多，查找命令的事件复杂度越高，可以使用map去解决该问题

# 15. 服务器解耦网络模块和业务逻辑

## 15.1 网络模块中解耦业务：将网络的初始化等细节放到Socket类里去实现，封装好这些细节的同时保留一个业务回调接口，以便网络去执行该回调，而不用管该回调的具体。

![Untitled](README_image/Untitled%208.png)

## 15.2 业务模块中解耦网络模块：只保留获取数据包，和向数据包列表添加包，其余的统统迁移到网络模块去执行

# 16. MVC设计模式实现客户端

MVC（Model-View-Controller）是一种常用的软件架构设计模式，用于组织应用程序的代码结构。它将应用程序分为三个主要部分：模型（Model）、视图（View）和控制器（Controller）。每个部分都有其特定的职责和功能，它们之间通过明确定义的接口进行交互，以实现代码的模块化和解耦合。

下面简要介绍一下 MVC 模式中每个部分的作用：

1. **模型（Model）：**
    - 模型负责表示应用程序的数据和业务逻辑。
    - 它通常包含数据的存储、检索、更新和验证等功能。
    - 模型与视图和控制器是相互独立的，不直接与用户界面交互。
2. **视图（View）：**
    - 视图**负责用户界面的展示和呈现**。
    - 它通常是用户可以看到和交互的部分，如图形界面、网页或其他用户界面。
    - 视图通常直接从模型中获取数据以进行展示，**但它不直接修改数据**。
3. **控制器（Controller）：**
    - 控制器是**模型和视图之间的中介**，负责处理用户的输入、数据流向和业务逻辑。
    - 它接收用户的输入（如点击按钮、输入文本等），并根据输入更新模型或视图。
    - 控制器通常将用户的输入转换为对模型的操作，并更新视图以反映模型的变化。

MVC 模式的优点包括：

- 分离关注点：将应用程序分为模型、视图和控制器，使得每个部分都可以独立地开发、测试和维护，降低了代码的耦合度。
- 可复用性：通过模块化的设计，可以更容易地重用模型、视图和控制器，以及它们之间的交互逻辑。
- 易于维护：清晰的分层结构和明确定义的接口使得代码更易于理解、修改和扩展。

然而，MVC 也有一些缺点，例如视图和控制器之间的耦合度可能较高，导致在修改视图时需要同时修改控制器；另外，如果不恰当地使用，可能会导致控制器变得臃肿，难以维护。

## 16.1 设计背景

主窗口CRemoteClientDlg对象基本上承载了所有的业务逻辑，实际上业务逻辑应该和ui对象解耦合，不应该深度绑定。视图层v是生灭的，而控制层应该贯穿整个生命周期的始终，即数据的生命周期应该比视图层更长。

## 16.2 设计思路

让主窗口对象CRemoteClientDlg成为Controller对象的成员变量，让Controller对象去调用业务(数据)逻辑接口(主要是网络接口)，而窗口函数调用Controller对象封装好的接口，间接调用业务数据。

Controller将能够管理所有页面数据，同时控制页面的创造和销毁。

# 17. 网络多线程发送问题(客户端)

## 17.1 具体问题：客户端的多线程发送请求，得到服务端的错乱回复(即A线程的回复被B线程收到这类问题)

- **解决不同线程同时调用send出现资源竞争问题**

将发送操作放入消息队列中，然后由消息处理函数来执行发送操作并等待接收，这是一种常见的解决多线程访问冲突的方法，通常称为**消息传递模式或者消息队列模式**。

这种方法的基本思想是，将需要在不同线程间共享的任务或操作封装成消息（Message），然后将这些消息发送到一个线程安全的消息队列中。每个线程都有一个专门的消息队列，它会轮询队列并处理接收到的消息。

通过这种方式，可以避免直接在多个线程中共享资源或访问同一个接口，从而避免了线程间的竞态条件和访问冲突。

总的来说，将发送操作放入消息队列中，可以一定程度上解决多线程访问冲突的问题。但是需要确保消息队列的实现是线程安全的，以及消息处理函数的执行是快速的，否则可能会引入新的问题，比如消息处理堆积、消息处理顺序混乱等。

- **而收到客户端回复的必定是请求需要的内容(请求使用了消息队列)，同样使用消息传递(SendMessage)，窗口绑定号消息处理函数。只要选择好正确的窗口句柄就可以收到正确回复！**

## 17.2 线程同步

`CreateEvent` 是 Windows API 中的一个函数，用于创建一个事件对象。

```cpp
HANDLE CreateEvent(
  LPSECURITY_ATTRIBUTES lpEventAttributes, // 安全属性，通常为 NULL
  BOOL                  bManualReset,       // 手动重置标志，TRUE 表示手动重置，FALSE 表示自动重置
  BOOL                  bInitialState,      // 初始状态，TRUE 表示已触发，FALSE 表示未触发
  LPCTSTR               lpName              // 事件对象的名称，通常为 NULL
);

```

- `lpEventAttributes`：指向 SECURITY_ATTRIBUTES 结构体的指针，用于指定事件对象的安全属性。通常情况下，设置为 `NULL`，表示使用默认的安全属性。
- `bManualReset`：指定事件对象的重置类型。如果为 `TRUE`，表示事件是手动重置的，即调用 `ResetEvent` 才能将事件重置为无信号状态；如果为 `FALSE`，表示事件是自动重置的，即当一个线程等待事件并且事件被触发后，事件会自动重置为无信号状态。
- `bInitialState`：指定事件对象的初始状态。如果为 `TRUE`，表示事件对象已经处于触发状态；如果为 `FALSE`，表示事件对象处于未触发状态。
- `lpName`：事件对象的名称。可以用于跨进程间共享事件对象。如果为 `NULL`，则表示事件对象是匿名的。

函数返回一个事件对象的句柄（`HANDLE` 类型），用于后续操作该事件对象。如果函数调用失败，则返回 `NULL`。

`CreateEvent` 函数通常用于多线程编程中，用于创建一个同步对象，**可以用于线程之间的通信和同步**。事件对象通常用于一个线程等待另一个线程的信号或通知。

# 18. 服务端执行权限和开机启动

## 18.1 UAC执行级别

![Untitled](README_image/Untitled%209.png)

UAC（User Account Control，用户帐户控制）执行级别是指 Windows 操作系统中用于控制程序以何种权限运行的一种机制。在 UAC 启用的情况下，当用户尝试运行某个程序时，Windows 会根据该程序的 UAC 执行级别来确定以何种权限运行该程序。

UAC 执行级别通常包括以下几种：

1. **最高权限（Highest Available）：** 程序将以当前用户的最高权限（通常是管理员权限）运行。如果当前用户是管理员，程序将以管理员权限运行；如果当前用户不是管理员，系统会提示用户输入管理员凭据来提升权限。
2. **必须以管理员身份运行（RequireAdministrator）：** 程序必须以管理员权限运行。如果当前用户不是管理员，系统会提示用户输入管理员凭据来提升权限。
3. **不要求管理员权限（AsInvoker）：** 程序将以当前用户的普通权限（非管理员权限）运行，不会提升权限。
4. **低权限（Lowest）：** 程序将以最低权限运行，即以系统内置的 Guest 账户权限运行。

UAC 执行级别的设置通常由程序的开发者在程序的清单文件（manifest）中指定。通过适当地设置 UAC 执行级别，可以确保程序在运行时以适当的权限运行，避免了过度的权限提升，从而提高了系统的安全性和稳定性。

## 18.2 开机启动

[windows对应用添加开机自启，实现自动运行_怎么添加自动启动项-CSDN博客](https://blog.csdn.net/positiving/article/details/135082922)

## 18.3 管理员运行权限检测、获取

![Untitled](README_image/Untitled%2010.png)

### `CheckTokenMembership`

在 Windows 编程中，可以使用一些 API 来检查当前程序是否以管理员权限运行。其中，最常用的方法是使用 `CheckTokenMembership` 函数。以下是一个简单的示例代码：

```cpp
#include <Windows.h>
#include <iostream>

bool IsRunAsAdmin() {
    BOOL bIsAdmin = FALSE;
    SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
    PSID AdminGroup;
    if (AllocateAndInitializeSid(&NtAuthority, 2,
        SECURITY_BUILTIN_DOMAIN_RID,
        DOMAIN_ALIAS_RID_ADMINS,
        0, 0, 0, 0, 0, 0,
        &AdminGroup)) {
        if (!CheckTokenMembership(NULL, AdminGroup, &bIsAdmin)) {
            bIsAdmin = FALSE;
        }
        FreeSid(AdminGroup);
    }
    return (bIsAdmin == TRUE);
}

int main() {
    if (IsRunAsAdmin()) {
        std::cout << "The program is running with administrator privileges." << std::endl;
    } else {
        std::cout << "The program is not running with administrator privileges." << std::endl;
    }
    return 0;
}

```

在这个示例中，`IsRunAsAdmin()` 函数会检查当前程序是否以管理员权限运行。它首先创建一个 SID（Security Identifier），表示管理员组，然后调用 `CheckTokenMembership` 函数来检查当前进程是否属于管理员组。如果当前进程属于管理员组，那么返回 `true`，表示以管理员权限运行；否则返回 `false`，表示未以管理员权限运行。

这种方法比较可靠，因为它会直接检查当前进程的权限，而不是简单地依赖于进程的文件属性或者其他方式。

![Untitled](README_image/Untitled%2011.png)

### `LogonUser`

`LogonUser` 函数可以用于以指定用户的凭据登录系统，并获取相应的访问令牌（access token）。你可以使用这个函数来获取管理员权限。

以下是一个简单的示例代码，演示如何使用 `LogonUser` 函数获取管理员权限：

```cpp
#include <iostream>
#include <windows.h>
#include <wincred.h>

bool GetAdminToken(HANDLE& hToken) {
    LPCWSTR username = L"Administrator"; // 替换成管理员用户名
    LPCWSTR domain = NULL; // 如果是本地用户，设置为 NULL
    LPCWSTR password = L"YourAdminPassword"; // 替换成管理员密码

    // LogonUser 函数登录指定用户，并返回访问令牌
    if (!LogonUser(username, domain, password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, &hToken)) {
        std::cerr << "LogonUser failed with error code: " << GetLastError() << std::endl;
        return false;
    }

    return true;
}

int main() {
    HANDLE hToken;

    if (GetAdminToken(hToken)) {
        std::cout << "Successfully obtained administrator token." << std::endl;
        // 这里可以使用 hToken 执行需要管理员权限的操作
        CloseHandle(hToken); // 使用完毕后记得关闭句柄
    } else {
        std::cerr << "Failed to obtain administrator token." << std::endl;
    }

    return 0;
}

```

在这个示例中，`GetAdminToken` 函数尝试使用 `LogonUser` 函数以管理员权限登录系统，并获取管理员权限的访问令牌。如果登录成功，则返回 `true`，并将令牌保存在 `hToken` 句柄中；如果登录失败，则返回 `false`。

在实际使用中，请务必替换示例代码中的管理员用户名和密码为实际的用户名和密码。并且要特别注意安全性，不要在代码中硬编码密码或敏感信息，最好采用安全的存储方式。

![Untitled](README_image/Untitled%2012.png)

# 19. 线程同步

## 19.1 同步手段

- 互斥锁
- 消息队列
- 本地回环网络或则内部局域网

# 20. 具有线程安全的队列实现

串行的程序只用到单个 CPU 核心， 希望加速整个程序， 考虑使用多线程加速。典型情况下可以找到生产者、消费者，两个角色之间通过队列进行数据交互：

- 生产者负责把数据放入队列Q
- 消费者负责从队列取出数据Q

要求队列是线程安全的，即：不能有读写冲突等。

[C++：设计一个线程安全的队列_c++ 线程安全队列-CSDN博客](https://blog.csdn.net/baiyu33/article/details/130663786?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522171345948116800225563881%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=171345948116800225563881&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~baidu_landing_v2~default-1-130663786-null-null.142^v100^pc_search_result_base9&utm_term=C%2B%2B%E7%BA%BF%E7%A8%8B%E5%AE%89%E5%85%A8%E7%9A%84%E9%98%9F%E5%88%97&spm=1018.2226.3001.4187)

[C++实现线程安全的队列_c++ queue线程安全吗-CSDN博客](https://blog.csdn.net/u011726005/article/details/82670730?ops_request_misc=&request_id=&biz_id=102&utm_term=C++%E7%BA%BF%E7%A8%8B%E5%AE%89%E5%85%A8%E7%9A%84%E9%98%9F%E5%88%97&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-5-82670730.142^v100^pc_search_result_base9&spm=1018.2226.3001.4187)

# 21. 高性能异步IO操作

## 21.1 优化服务端只能每次处理1个命令请求

[IOCP编程](README_image/IOCP%E7%BC%96%E7%A8%8B%20ea601fbec73941768628ae4208370215.md)

# 22. UDP穿透技术

[UDP穿透-CSDN博客](https://blog.csdn.net/weixin_54720578/article/details/130435641?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522171360293216800227443673%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=171360293216800227443673&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-1-130435641-null-null.142^v100^pc_search_result_base9&utm_term=udp%E7%A9%BF%E9%80%8F&spm=1018.2226.3001.4187)

[UDP内网穿透和打洞原理与代码实现_udp穿透原理-CSDN博客](https://blog.csdn.net/qq_40989769/article/details/135003318)

通过一个公网服务器把A，B客户端的公网地址获取到，并让A，B相互转发

# 下一步

## 4月6日

1、修复发送的是packet类对象而不是数据的问题：添加函数方法、类对象对齐？？

2、完成调试函数，并熟悉调试手段(完成)

3、总之，将查看本地磁盘分区，并把信息打包成数据包整个过程调试完成并熟悉调试手段

## 4月10日

1、接收的buffer改成vector数组防止内存泄漏？

## 4月11日

1、客户端请求D盘文件数据，服务端解包出现问题？

=运算符重载，赋值strdata错误了，改正即可

![Untitled](README_image/Untitled%2013.png)

**2、客户端显示的D盘的文件内容和数量都不对？**

- 先查看服务端发送D盘的内容和数量：调试发现服务端发送D盘的内容和数量均没有问题
- 重点排查客户端的逻辑：

![Untitled](README_image/Untitled%2014.png)

问题原因：解包出错。服务端数据发完后，客户端收到粘连的数据包就因为recv函数阻塞而无法解析

解决：先把收到的数据完全解析完，再recv。

![Untitled](README_image/Untitled%2015.png)

## 4月13日

创建线程去执行下载任务，报错：

![Untitled](README_image/Untitled%2016.png)

**问题根源是创建的线程和主线程可能会争抢资源，系统机制做出了阻断，实际上只有主线程可以去使用资源，所以只需要创建的线程去发送消息给主线程去执行即可。**

解决：副线程去发送消息给主线程，主线程添加消息ID，写一个消息处理函数即可。

[下载任务线程去发送消息给主线程执行SendCommandPacket · 17b90a9 · YinaYan/RemoteControl - Gitee.com](https://gitee.com/YinaYan/remote-control/commit/17b90a94b2d0b19b3c0af009fcddb6b0b3408682)

![Untitled](README_image/Untitled%2017.png)

![Untitled](README_image/Untitled%2018.png)

# 4月18日

![Untitled](README_image/Untitled%2019.png)