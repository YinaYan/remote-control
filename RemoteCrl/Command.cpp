﻿#include "pch.h"
#include "Command.h"

CCommand::CCommand():hThread(0)
{
	struct 
	{
		int nCmd;
		CMDFUNC func;
	}data[] = {
		{1,&CCommand::MakeDriverInfo},
		{2,&CCommand::MakeDirectoryInof},
		{3,&CCommand::RunFile},
		{4,&CCommand::DownloadFile},
		{5,&CCommand::MouseEvent},
		{6,&CCommand::SendScreen},
		{7,&CCommand::LockMachine},
		{8,&CCommand::UnLockMachine},
		{9,&CCommand::DeleteLocalFile},
		{309,&CCommand::TestConnect},
		{-1,NULL},
	};
	for (int i = 0; data[i].nCmd!=-1; i++)
	{
		m_mapFunction.insert(std::pair<int, CMDFUNC>(data[i].nCmd, data[i].func));
	}
}

int CCommand::ExcuteCommand(int nCmd, std::list<Packet>& lisPackets, const Packet cmdPackets)
{
    // 查找具体的业务
	auto it = m_mapFunction.find(nCmd);
	if (it == m_mapFunction.end()) {
		return -1;
	}
    // 调用具体的业务
	return (this->*(it->second))(lisPackets, cmdPackets);
}

unsigned __stdcall CCommand::ThreadLock(void* param)
{
    CCommand* thiz = (CCommand*)param;
    thiz->Lock();
    _endthreadex(0);
    return 0;
}

void CCommand::RunCommand(void* arg, int status, std::list<Packet>& lisPackets, const Packet cmdPackets)
{
    CCommand* thiz = (CCommand*)arg;
    if (status > 0) {
        int ret = thiz->ExcuteCommand(status, lisPackets, cmdPackets);
        if (ret != 0) {
            TRACE("执行命令失败：%d ret = %d\r\n", status, ret);
        }
    }
    else {
        MessageBox(NULL, _T("无法正常接入用户，自动重试！"), _T("接入用户失败！"), MB_OK | MB_ICONERROR);

    }
}

void CCommand::Lock()
{
    // 创造非模态对话框 绑定对话框模板资源id
    dlg.Create(IDD_DIALOG_INFO, NULL);
    // 将窗口显示在屏幕上并激活
    dlg.ShowWindow(SW_SHOW);

    // 调整遮蔽后台为全屏
    CRect rect;
    //rect.left = 0;
    //rect.top = 0;
    //rect.right = GetSystemMetrics(SM_CXFULLSCREEN);//w1
    //rect.bottom = GetSystemMetrics(SM_CYFULLSCREEN);
    //rect.bottom = LONG(rect.bottom * 1.10);
    //TRACE("right = %d bottom = %d\r\n", rect.right, rect.bottom);
    //dlg.MoveWindow(rect);

    // 将 dlg 对话框对象设置为顶层窗口
    dlg.SetWindowPos(&dlg.wndTopMost, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);

    // 设置鼠标不可见
    ShowCursor(false);
    // 限制鼠标移动范围在dlg内
    dlg.GetWindowRect(rect);
    ClipCursor(rect);

    // 消息循环
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
        if (msg.message == WM_KEYDOWN) {    // 键盘按下
            //TRACE("msg:%08X wparam:%08x lparam:%08X\r\n", msg.message, msg.wParam, msg.lParam);
            if (msg.wParam == 0x1B) {   // Esc键按下，退出循环
                break;
            }
        }
    }
    ClipCursor(NULL);
    dlg.DestroyWindow();
    // 恢复鼠标为可见
    ShowCursor(true);
}

int CCommand::RunFile(std::list<Packet>& lisPackets, const Packet cmdPackets)
{
	std::string strPath = cmdPackets.strData;
	// 相当于鼠标双击
	ShellExecuteA(NULL, NULL, strPath.c_str(), NULL, NULL, SW_SHOWNORMAL);
	// 执行成功回复
    lisPackets.push_back(Packet(3, NULL, 0));

	return 0;
}

int CCommand::MouseEvent(std::list<Packet>& lisPackets, const Packet cmdPackets)
{
    MOUSEEV mouse;
    memcpy((void*)&mouse, cmdPackets.strData.c_str(), sizeof(mouse));
    DWORD nFlags = 0;
    switch (mouse.nButton) {
    case 0: // 左键
        nFlags = 1;
        break;
    case 1: // 右键
        nFlags = 2;
        break;
    case 2: // 中键
        nFlags = 4;
    case 8: // 没有按键
        nFlags = 8;
    }
    if (nFlags != 8) {
        // 设置鼠标坐标
        SetCursorPos(mouse.ptXY.x, mouse.ptXY.y);
    }
    // 使用nFlags的第3,4位
    switch (mouse.nAction)
    {
    case 0: // 单击
        nFlags |= 0x10;
        break;
    case 1: // 双击
        nFlags |= 0x20;
        break;
    case 2: // 按下
        nFlags |= 0x40;
        break;
    case 3: // 放开
        nFlags |= 0x80;
        break;
    default:
        break;
    }

    // 避免嵌套nButton和nFlags嵌套
    switch (nFlags)
    {
    case 0x21:  // 左键双击
        mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x11:  // 左键单击
        mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x41:  // 中键按下
        mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x81:  // 左键放开
        mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x22:  // 右键双击
        mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x12:  // 右键单击
        mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x42:  // 右键按下
        mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x82:  // 右键放开
        mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x24:  // 中键双击
        mouse_event(MOUSEEVENTF_MIDDLEDOWN, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_MIDDLEUP, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_MIDDLEDOWN, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_MIDDLEUP, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x14:  // 中键单击
        mouse_event(MOUSEEVENTF_MIDDLEDOWN, 0, 0, 0, GetMessageExtraInfo());
        mouse_event(MOUSEEVENTF_MIDDLEUP, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x44:  // 中键按下
        mouse_event(MOUSEEVENTF_MIDDLEDOWN, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x84:  // 中键放开
        mouse_event(MOUSEEVENTF_MIDDLEUP, 0, 0, 0, GetMessageExtraInfo());
        break;
    case 0x18:  // 移动鼠标
    {
        // 此接口在最新的win11系统上无法起作用，换成如下接口
        //mouse_event(MOUSEEVENTF_MOVE, mouse.ptXY.x, mouse.ptXY.y, 0, GetMessageExtraInfo());
        MoveMouseTo(mouse.ptXY.x, mouse.ptXY.y);
    }
    break;
    default:
        break;
    }
    lisPackets.push_back(Packet(5, NULL, 0));
	return 0;
}

int CCommand::DownloadFile(std::list<Packet>& lisPackets, const Packet cmdPackets)
{
    std::string strPath =  cmdPackets.strData;
    long long data = 0;
    FILE* pFile = NULL;

    // 打开文件
    errno_t err = fopen_s(&pFile, strPath.c_str(), "rb");
    if (err != 0) {
        lisPackets.push_back(Packet(4, (BYTE*)&data, 8));
        return -1;
    }

    if (pFile != NULL) {
        // 发送文件大小
        // 将文件指针设置到末尾
        fseek(pFile, 0, SEEK_END);
        // 获取文件指针偏移量（字节）
        data = _ftelli64(pFile);
        lisPackets.push_back(Packet(4, (BYTE*)&data, 8));

        //  将文件指针设置到开始
        fseek(pFile, 0, SEEK_SET);

        // 发送文件
        // 每1024字节循环发送
        char buffer[1024] = { 0 };
        size_t rlen = 0;
        do {
            rlen = fread(buffer, 1, 1024, pFile);
            lisPackets.push_back(Packet(4, (BYTE*)buffer, rlen));

        } while (rlen >= 1024);
        fclose(pFile);
    }
    lisPackets.push_back(Packet(4, NULL, 0));

    return 0;
}

int CCommand::DeleteLocalFile(std::list<Packet>& lisPackets, const Packet cmdPackets)
{
    std::string strPath = cmdPackets.strData;
    TCHAR sPath[MAX_PATH] = _T("");
    // 字符集变化
    MultiByteToWideChar(CP_ACP, 0, strPath.c_str(), strPath.size(), sPath, sizeof(sPath) / sizeof(TCHAR));
    DeleteFileA(strPath.c_str());
    lisPackets.push_back(Packet(9, NULL, 0));
    return 0;
}

int CCommand::MakeDriverInfo(std::list<Packet>& lisPackets, const Packet cmdPackets)
{
    std::string result;
    for (int i = 1; i < 26; i++) {
        if (_chdrive(i) == 0) {
            if (result.size() > 0) {
                result += ',';
            }
            result += 'A' + i - 1;
        }
    }
    //CTools::Dump((BYTE*)&packet, packet.Size());
    //CTools::Dump((BYTE*)packet.Data(), packet.Size());
    // 发送消息给客户端

    lisPackets.push_back(Packet(1, (BYTE*)result.c_str(), result.size()));
    return 0;
}

int CCommand::MakeDirectoryInof(std::list<Packet>& lisPackets, const Packet cmdPackets)
{
    std::string strPath = cmdPackets.strData;

    // 切换到指定目录
    if (_chdir(strPath.c_str()) != 0) {
        FILEINFO fileInfo;
        fileInfo.HasNext = FALSE;
        lisPackets.push_back(Packet(2, (BYTE*)&fileInfo, sizeof(fileInfo)));
        OutputDebugString(_T("没有权限，访问目录！！"));
        return -2;
    }

    // 查找文件
    intptr_t handle = 0;
    struct _finddata_t fileinfo;

    // 在指定目录下查找第一个文件
    handle = _findfirst("*", &fileinfo);
    if (handle == -1) {
        FILEINFO finfo;
        finfo.HasNext = FALSE;
        lisPackets.push_back(Packet(2, (BYTE*)&finfo, sizeof(finfo)));
        OutputDebugString(_T("没有找到任何文件"));
        return -3;
    }

    do {
        FILEINFO finfo;
        finfo.IsDirectory = (fileinfo.attrib & _A_SUBDIR) != 0;
        memcpy(finfo.szFileName, fileinfo.name, strlen(fileinfo.name));

        lisPackets.push_back(Packet(2, (BYTE*)&finfo, sizeof(finfo)));


    } while (_findnext(handle, &fileinfo) == 0);    // 继续查找下一个文件

    // 告知客户端没有文件了
    FILEINFO finfo;
    finfo.HasNext = FALSE;
    lisPackets.push_back(Packet(2, (BYTE*)&finfo, sizeof(finfo)));

    // 关闭文件查找句柄
    _findclose(handle);
    return 0;
}

int CCommand::SendScreen(std::list<Packet>& lisPackets, const Packet cmdPackets)
{
    CImage screen;//GDI
    // 获取整个屏幕的设备上下文（Device Context）       
    HDC hScreen = ::GetDC(NULL);
    // 获取屏幕的位深度，即每个像素所占的位数。
    int nBitPerPixel = GetDeviceCaps(hScreen, BITSPIXEL);//24   ARGB8888 32bit RGB888 24bit RGB565  RGB444
    // 获取屏幕的水平分辨率，即屏幕宽度
    int nWidth = GetDeviceCaps(hScreen, HORZRES);
    // 获取屏幕的垂直分辨率，即屏幕高度。
    int nHeight = GetDeviceCaps(hScreen, VERTRES);
    // 创建一个 CImage 对象，大小与屏幕分辨率相同，并且位深度与屏幕一致。
    screen.Create(nWidth, nHeight, nBitPerPixel);
    // 使用 BitBlt 函数将屏幕上的图像复制到 screen 对象中。
    BitBlt(screen.GetDC(), 0, 0, nWidth, nHeight, hScreen, 0, 0, SRCCOPY);
    // 释放屏幕的设备上下文
    ReleaseDC(NULL, hScreen);

    //screen.Save(_T("0408.png"), Gdiplus::ImageFormatPNG);

    // 将图片保存到内存中，便于发送 
    // 分配一个可移动的全局内存块
    HGLOBAL hMem = GlobalAlloc(GMEM_MOVEABLE, 0);
    if (hMem == NULL)return -1;

    // 创建一个基于全局内存的数据流对象，并将其与全局内存块关联。
    IStream* pStream = NULL;
    HRESULT ret = CreateStreamOnHGlobal(hMem, TRUE, &pStream);

    if (ret == S_OK) {
        // 将 screen 对象中的图像保存为 PNG 格式，并写入到全局内存中。
        screen.Save(pStream, Gdiplus::ImageFormatPNG);
        // 将数据流的指针移动到起始位置。
        LARGE_INTEGER bg = { 0 };
        pStream->Seek(bg, STREAM_SEEK_SET, NULL);
        // 锁定全局内存块，并返回指向内存块的指针。
        PBYTE pData = (PBYTE)GlobalLock(hMem);
        // 获取全局内存块的大小。
        SIZE_T nSize = GlobalSize(hMem);
        // 打包到packet中,发送客户端
        lisPackets.push_back(Packet(6, pData, nSize));
        // 解锁全局内存块
        GlobalUnlock(hMem);
    }
    // 释放数据流对象
    pStream->Release();
    // 释放全局内存块
    GlobalFree(hMem);
    // 释放 screen 对象的设备上下文
    screen.ReleaseDC();
    return 0;
}

int CCommand::LockMachine(std::list<Packet>& lisPackets, const Packet cmdPackets)
{
    // 防止反复创建线程
    if ((dlg.m_hWnd == NULL) || (dlg.m_hWnd == INVALID_HANDLE_VALUE)) {
        //_beginthreadex(NULL, 0, &CCommand::threadLockDlg, this, 0, &threadid);
        //TRACE("threadid=%d\r\n", threadid);
        // 创建线程
        hThread = (HANDLE)_beginthreadex(NULL, 0, &ThreadLock, this, 0, NULL);
        lisPackets.push_back(Packet(7, NULL, 0));
        if (hThread == NULL) {
            // 创建线程失败
            TRACE("creat thread for LockMachine failed \r\n");
            return -1;
        }
    }
    return 0;
}

int CCommand::UnLockMachine(std::list<Packet>& lisPackets, const Packet cmdPackets)
{
    // 向线程发送消息
    PostThreadMessage(GetThreadId(hThread), WM_KEYDOWN, 0x1B, 0);
    lisPackets.push_back(Packet(8, NULL, 0));
    return 0;
}

int CCommand::TestConnect(std::list<Packet>& lisPackets, const Packet cmdPackets)
{
    TRACE("recv client and send!\r\n");
    lisPackets.push_back(Packet(309, NULL, 0));
    return 0;
}

void CCommand::MoveMouseTo(int x, int y)
{
	// 获取屏幕宽度和高度
	int screenWidth = GetSystemMetrics(SM_CXSCREEN);
	int screenHeight = GetSystemMetrics(SM_CYSCREEN);

	// 创建一个INPUT结构体数组，包含一个鼠标移动事件
	INPUT input[1];
	input[0].type = INPUT_MOUSE;
	input[0].mi.dx = x * (65535.0f / screenWidth); // 计算屏幕坐标
	input[0].mi.dy = y * (65535.0f / screenHeight); // 计算屏幕坐标
	input[0].mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE;
	input[0].mi.mouseData = 0;
	input[0].mi.dwExtraInfo = 0;
	input[0].mi.time = 0;

	// 发送输入事件
	SendInput(1, input, sizeof(INPUT));
}
