﻿#include "pch.h"
#include "Packet.h"

#define HEAD_DATA 0xFEFF

Packet::Packet() :sHead(0), nLength(0), sCmd(0), sSum(0)
{
}

Packet::Packet(const Packet& pack)
{
	sHead = pack.sHead;
	nLength = pack.nLength;
	sCmd = pack.sCmd;
	strData = pack.strData;
	sSum = pack.sSum;
}

Packet::Packet(const BYTE* pData, size_t& nSize) :sHead(0), nLength(0), sCmd(0), sSum(0)
{
	size_t i = 0;
	size_t sHeadLen = sizeof(sHead);
	size_t nLengthLen = sizeof(nLength);
	size_t sCmdLen = sizeof(sCmd);
	size_t sSumLen = sizeof(sSum);

	for (; i < nSize; i++) {
		if (*(WORD*)(pData + i) == HEAD_DATA) {
			sHead = HEAD_DATA;
			i += sHeadLen;
			break;
		}
	}
	if (i + nLengthLen + sCmdLen + sSumLen > nSize) {
		// 包未完全收到，或者包头未能全部接收
		nSize = 0;
		return;
	}
	nLength = *(DWORD*)(pData + i);
	i += nLengthLen;
	if (nLength + i > nSize) {
		// 包未完全接收到，就返回，解析失败
		nSize = 0;
		return;
	}
	sCmd = *(WORD*)(pData + i);
	i += sCmdLen;

	if (nLength > sCmdLen + sSumLen) {
		size_t strDataLen = nLength - sCmdLen - sSumLen;
		strData.resize(strDataLen);
		memcpy((void*)strData.c_str(), pData + i, strDataLen);
		i += strDataLen;
	}
	sSum = *(WORD*)(pData + i);
	i += sSumLen;

	// 和校验
	WORD sum = 0;
	for (size_t j = 0; j < strData.size(); j++)
	{
		sum += BYTE(strData[j]) & 0xFF;
	}
	if (sum == sSum) {
		nSize = i;
		return;
	}
	nSize = 0;
}

Packet::Packet(WORD nCmd, const BYTE* pData, size_t nSize)
	:sHead(HEAD_DATA), sCmd(nCmd), sSum(0), nLength(nSize + sizeof(sCmd) + sizeof(sSum))
{
	if (nSize > 0) {
		strData.resize(nSize);
		memcpy((void*)strData.c_str(), pData, nSize);
	}
	else {
		strData.clear();
	}

	for (size_t j = 0; j < strData.size(); j++) {
		sSum += BYTE(strData[j]) & 0xFF;
	}
}

Packet& Packet::operator=(const Packet& pack)
{
	if (this != &pack) {
		sHead = pack.sHead;
		nLength = pack.nLength;
		sCmd = pack.sCmd;
		strData = pack.strData;
		sSum = pack.sSum;
	}
	return *this;
}

size_t Packet::Size() const
{
	return nLength + sizeof(sHead) + sizeof(nLength);
}

const char* Packet::Data()
{

	strOut.resize(Size());
	BYTE* pData = (BYTE*)strOut.c_str();
	*(WORD*)pData = sHead; pData += sizeof(sHead);
	*(DWORD*)(pData) = nLength; pData += sizeof(nLength);
	*(WORD*)pData = sCmd; pData += sizeof(sCmd);
	memcpy(pData, strData.c_str(), strData.size()); pData += strData.size();
	*(WORD*)pData = sSum;

	return strOut.c_str();
}

Packet::~Packet()
{
}
