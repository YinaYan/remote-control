﻿#pragma once
#include <iostream>

#pragma pack(push, 1)	// 保存当前对齐设置，并设置对齐值为 1 字节
class Packet
{
public:
	Packet();
	Packet(const Packet& pack);
	// 解包构造
	Packet(const BYTE* pData, size_t& nSize);
	// 打包构造
	Packet(WORD nCmd, const BYTE* pData, size_t nSize);
	Packet& operator=(const Packet& pack);
	size_t Size() const;
	const char* Data();
	~Packet();

public:
	WORD sHead;          // 固定位FE FF 2字节
	DWORD nLength;       // 包长度 从控制命令到和校验结束 4
	WORD sCmd;           // 控制命令 2
	std::string strData; // 包数据
	WORD sSum;           // 和校验 2
	std::string strOut;	// 整个数据包的数据
};
#pragma pack(pop) // 恢复之前的对齐设置

// 电脑磁盘文件信息
typedef struct file_info {
	file_info() {
		IsInvalid = FALSE;
		IsDirectory = -1;
		HasNext = TRUE;
		memset(szFileName, 0, sizeof(szFileName));
	}
	BOOL IsInvalid;	// 目录(文件)是否有效 
	BOOL IsDirectory;	//目录？
	BOOL HasNext;	//	有下级目录？
	char szFileName[256];	// 目录(文件)名
}FILEINFO, * PFILEINFO;

// 鼠标坐标及事件
typedef struct MouseEvent {
	MouseEvent() {
		nAction = 0;
		nButton = -1;
		ptXY.x = 0;
		ptXY.y = 0;
	}
	WORD nAction;// 点击、移动、双击
	WORD nButton;// 左键、右键、中键
	POINT ptXY;// 坐标
}MOUSEEV, * PMOUSEEV;