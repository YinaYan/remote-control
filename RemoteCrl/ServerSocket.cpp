﻿#include "pch.h"
#include "ServerSocket.h"
#include <list>
#define BUFFER_SIZE 4096 * 100 *100
CServerSocket* CServerSocket::m_instance = nullptr;
CServerSocket::CHelper CServerSocket::m_helper;


int CServerSocket::Run(SOCKET_CALLBACK callback, void* arg, short port)
{
	if (!InitSocket(port)) {
		return -1;
	}
	std::list<Packet> lstPackets;
	m_callBack = callback;
	m_arg = arg;
	int count = 0;
	while (true)
	{
		if (!AcceptClient()) {
			if (count >= 3) {
				return -2;
			}
			count++;
		}
		// 获取数据包并解析
		int ret = DealCommand();

		// 根据获取的命令去执行业务逻辑
		m_callBack(m_arg, ret, lstPackets, m_packet);

		// 发送数据包给客户端
		while (!lstPackets.empty())
		{
			if (!Send(lstPackets.front()))
			{
				TRACE("m_clientSocket is error \r\n");
				break;
			}
			lstPackets.pop_front();
		}
		//CloseClient();
	}
}

bool CServerSocket::InitSocket(short port)
{
	// // 创建套接字
	m_serv_sock = socket(PF_INET, SOCK_STREAM, 0);
	if (m_serv_sock == INVALID_SOCKET) {
		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "Failed to create socket." << std::endl;
		return false;
	}

	// 绑定地址和端口
	sockaddr_in serv_adr;
	memset(&serv_adr, 0, sizeof(serv_adr));
	serv_adr.sin_family = AF_INET;
	serv_adr.sin_addr.s_addr = INADDR_ANY;
	serv_adr.sin_port = htons(port);
	if (bind(m_serv_sock, (sockaddr*)&serv_adr, sizeof(serv_adr)) == SOCKET_ERROR) {
		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "Failed to bind socket." << std::endl;
		closesocket(m_serv_sock);
		WSACleanup();
		return false;
	}

	// 监听连接请求
	if (listen(m_serv_sock, 1) == SOCKET_ERROR) {
		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "Listen failed." << std::endl;
		closesocket(m_serv_sock);
		WSACleanup();
		return false;
	}
	return true;
}

bool CServerSocket::AcceptClient()
{
	// 接受客户端连接
	sockaddr_in client_adr;
	int cli_sz = sizeof(client_adr);
	m_clientSocket = accept(m_serv_sock, (sockaddr*)&client_adr, &cli_sz);
	if (m_clientSocket == INVALID_SOCKET) {
		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "Accept failed." << std::endl;
		return false;
	}
	return true;
}

int CServerSocket::DealCommand()
{
	if (m_clientSocket == INVALID_SOCKET) {
		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "invalid client socket" << std::endl;
		return -1;
	}
	char* buffer = m_buffer.data();
	// 此处解决粘包问题的算法思路需要注意一下
	// 此处好像解析到一个完整包就返回结果终止了，如果粘包了，第二个包的较前段数据会丢弃？？？
	static size_t index = 0;
	while (true)
	{
		if (index > 0) {	// 有滞留数据，先把收到的数据完全解析完
			size_t bufferLen = index;
			m_packet = Packet((BYTE*)buffer, bufferLen);
			if (bufferLen > 0) {
				memmove(buffer, buffer + bufferLen, BUFFER_SIZE - bufferLen);
				index -= bufferLen;
				return m_packet.sCmd;
			}
		}
		size_t len = recv(m_clientSocket, buffer + index, BUFFER_SIZE - index, 0);
		if (len <= 0) {
			std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "recv error" << std::endl;
			return -1;
		}
		index += len;
		len = index;
		// =运算符重载
		m_packet = Packet((BYTE*)buffer, len);
		if (len > 0) {
			memmove(buffer, buffer + len, BUFFER_SIZE - len);
			index -= len;
			return m_packet.sCmd;
		}
	}
	return -1;
}

bool CServerSocket::Send(const char* pData, int nSize)
{
	if (m_clientSocket == INVALID_SOCKET) {
		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "invalid client socket" << std::endl;
		return false;
	}
	return send(m_clientSocket, pData, nSize, 0) > 0;
}

bool CServerSocket::Send(Packet& pack)
{
	if (m_clientSocket == -1)
		return false;
	return send(m_clientSocket, pack.Data(), pack.Size(), 0);
}

bool CServerSocket::GetFilePath(std::string& filePath)
{
	if (((m_packet.sCmd >= 2) && (m_packet.sCmd <= 4))||(m_packet.sCmd == 9)) {
		filePath = m_packet.strData;
		return true;
	}
	return false;
}

bool CServerSocket::GetMouseEvent(const MOUSEEV& mouse)
{
	if (m_packet.sCmd == 5) {
		memcpy((void*) & mouse, m_packet.strData.c_str(), sizeof(mouse));
		return true;
	}
	return false;
}

void CServerSocket::CloseClient()
{
	if (m_clientSocket != INVALID_SOCKET) {
		closesocket(m_clientSocket);
		m_clientSocket = INVALID_SOCKET;
	}
}

CServerSocket::CServerSocket() :m_clientSocket(INVALID_SOCKET)
{
	if (InitSocketEnv() == FALSE) {
		MessageBox(NULL, _T("无法初始化套接字环境,请检查网络设置"), _T("初始化错误"), MB_OK | MB_ICONERROR);
		exit(0);
	}
}

BOOL CServerSocket::InitSocketEnv() {
	// 初始化套接字
	m_buffer.resize(BUFFER_SIZE);
	memset(m_buffer.data(), 0, BUFFER_SIZE);
	WSADATA data;
	if (WSAStartup(MAKEWORD(1, 1), &data) != 0) {
		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "WSAStartu fail" << std::endl;
		return FALSE;
	}
	return TRUE;
}

CServerSocket::~CServerSocket()
{
	closesocket(m_serv_sock);
	WSACleanup();
}