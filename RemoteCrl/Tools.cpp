﻿#include "pch.h"
#include "Tools.h"

void CTools::Dump(BYTE* pData, size_t nSize)
{
    std::string strOut = "send ";
    for (size_t i = 0; i < nSize; i++)
    {
        char buf[64] = "";
        if (i > 0 && (i % 16 == 0))strOut += "\n";
        snprintf(buf, sizeof(buf), "%02X ", pData[i] & 0xFF);
        strOut += buf;
    }
    strOut += "\n";
    // 输出到调试
    OutputDebugStringA(strOut.c_str());
}
