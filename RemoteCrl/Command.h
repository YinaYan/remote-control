﻿#pragma once
#include <map>
#include "ServerSocket.h"
#include <direct.h>
#include <io.h>
#include <atlimage.h>
#include "LockInfoDialog.h"
#include <process.h>
#include "resource.h"
#include <list>
class CCommand	// 简化命令
{
public:
	CCommand();
	int ExcuteCommand(int nCmd,std::list<Packet>& lisPackets, const Packet cmdPackets);
	static void RunCommand(void* arg, int status, std::list<Packet>& lisPackets,  const Packet cmdPackets);
protected:
	static unsigned __stdcall ThreadLock(void* param);
	void Lock();
	// 运行文件
	int RunFile(std::list<Packet>& lisPackets, const Packet cmdPackets);
	// 鼠标事件
	int MouseEvent(std::list<Packet>& lisPackets, const Packet cmdPackets);
	// 下载文件
	int DownloadFile(std::list<Packet>& lisPackets, const Packet cmdPackets);
	// 删除文件
	int DeleteLocalFile(std::list<Packet>& lisPackets, const Packet cmdPackets);
	// 查看磁盘分区
	int MakeDriverInfo(std::list<Packet>& lisPackets, const Packet cmdPackets);
	// 查看指定目录下的文件
	int MakeDirectoryInof(std::list<Packet>& lisPackets, const Packet cmdPackets);
	// 发送屏幕信息
	int SendScreen(std::list<Packet>& lisPackets, const Packet cmdPackets);
	// 锁机
	int LockMachine(std::list<Packet>& lisPackets, const Packet cmdPackets);
	// 解锁
	int UnLockMachine(std::list<Packet>& lisPackets, const Packet cmdPackets);
	// 连接测试
	int TestConnect(std::list<Packet>& lisPackets, const Packet cmdPackets);
private:
	void MoveMouseTo(int x, int y);
protected:
	typedef int(CCommand::* CMDFUNC)(std::list<Packet>& lisPackets, const Packet cmdPackets);	// 成员函数指针(等待填充成员函数)
	std::map<int, CMDFUNC> m_mapFunction;
	CLockInfoDialog dlg;
	HANDLE hThread;
};

