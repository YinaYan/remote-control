﻿#pragma once
#include "pch.h"
#include "framework.h"
#include "Packet.h"
#include <vector>
#include <list>
#define PORT 9567
using namespace std;

typedef void (*SOCKET_CALLBACK)(void* arg,int ret, std::list<Packet>& lisPackets,const Packet cmdPackets);
class CServerSocket
{
public:
	static CServerSocket* GetInstance()
	{
		if (m_instance == nullptr)
		{
			m_instance = new CServerSocket();
		}
		return m_instance;
	}
	int Run(SOCKET_CALLBACK callback, void* arg, short port = PORT);
	bool InitSocket(short port);
	bool AcceptClient();
	// 接收客户端命令并处理命令
	int DealCommand();
	bool Send(const char* pData, int nSize);
	bool Send(Packet& pack);
	bool GetFilePath(std::string& filePath);
	bool GetMouseEvent(const MOUSEEV& mouse);
	void CloseClient();
private:
	CServerSocket();
	~CServerSocket();
	CServerSocket& operator=(const CServerSocket serverSocket) {}
	CServerSocket(CServerSocket& serverSocket) {
		m_serv_sock = serverSocket.m_serv_sock;
		m_clientSocket = serverSocket.m_clientSocket;
	}
	static void ReleaseInstance()
	{
		if (m_instance != nullptr)
		{
			delete m_instance;
			m_instance = nullptr;
		}
	}
	BOOL InitSocketEnv();

	class CHelper {
	public:
		CHelper() {
			CServerSocket::GetInstance();
		}
		~CHelper() {
			CServerSocket::ReleaseInstance();
		}
	};

private:
	static CHelper m_helper;
	static CServerSocket* m_instance;
	SOCKET m_serv_sock;
	SOCKET m_clientSocket;
	Packet m_packet;
	std::vector<char> m_buffer;
	SOCKET_CALLBACK m_callBack;
	void* m_arg;
};
