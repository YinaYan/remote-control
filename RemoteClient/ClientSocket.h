#pragma once
#include "Packet.h"
#include <string>
#include <vector>
class CClientSocket
{
public:
	static CClientSocket* GetInstance()
	{
		if (m_instance == nullptr)
		{
			m_instance = new CClientSocket();
		}
		return m_instance;
	}
	Packet& GetPacket() {
		return m_packet;
	}
	bool InitSocket(int nIP, int nPort);
	bool AcceptClient();
	// 接收客户端命令并处理命令
	int DealCommand();
	bool Send(const char* pData, int nSize);
	bool Send(Packet& pack);
	bool GetFilePath(std::string& filePath);
	bool GetMouseEvent(const MOUSEEV& mouse);
	void CloseSocket();
private:
	CClientSocket();
	~CClientSocket();
	CClientSocket& operator=(const CClientSocket serverSocket) {}
	CClientSocket(CClientSocket& serverSocket) {
		m_sock = serverSocket.m_sock;
	}
	static void ReleaseInstance()
	{
		if (m_instance != nullptr)
		{
			delete m_instance;
			m_instance = nullptr;
		}
	}
	BOOL InitSocketEnv();

	class CHelper {
	public:
		CHelper() {
			CClientSocket::GetInstance();
		}
		~CHelper() {
			CClientSocket::ReleaseInstance();
		}
	};

private:
	static CHelper m_helper;
	static CClientSocket* m_instance;
	SOCKET m_sock;
	Packet m_packet;
	std::vector<char> m_buffer;
};

