﻿#include "pch.h"
#include "ClientSocket.h"
#include <ws2tcpip.h>
#define PORT 9567
#define BUFFER_SIZE 4096 *100 *100
CClientSocket* CClientSocket::m_instance = nullptr;
CClientSocket::CHelper CClientSocket::m_helper;


std::string GetErrInfo(int wsaErrCode)
{
	std::string ret;
	LPVOID lpMsgBuf = NULL;
	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL,
		wsaErrCode,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	ret = (char*)lpMsgBuf;
	LocalFree(lpMsgBuf);
	return ret;
}

bool CClientSocket::InitSocket(int nIP,int nPort)
{
	// // 创建套接字
	m_sock = socket(PF_INET, SOCK_STREAM, 0);
	if (m_sock == INVALID_SOCKET) {
		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "Failed to create socket." << std::endl;
		return false;
	}

	// 连接服务端
	sockaddr_in serv_adr;
	memset(&serv_adr, 0, sizeof(serv_adr));
	serv_adr.sin_family = AF_INET;
	// 选择服务端ip
	serv_adr.sin_addr.s_addr = htonl(nIP);
	//serv_adr.sin_addr.s_addr = inet_addr(strIPAddress.c_str());
	//inet_pton(AF_INET, "127.0.0.1", &serv_adr.sin_addr);
	serv_adr.sin_port = htons(nPort);

	if (connect(m_sock, (sockaddr*)&serv_adr, sizeof(serv_adr)) == -1) {
		TRACE("连接失败：%d %s \r\n", WSAGetLastError(), GetErrInfo(WSAGetLastError()).c_str());
		AfxMessageBox("连接失败");
		//closesocket(m_sock);
		//WSACleanup();
		return false;
	}
	TRACE("socket init done!\r\n");
	return true;
}

//bool CClientSocket::AcceptClient()
//{
//	// 接受客户端连接
//	sockaddr_in client_adr;
//	int cli_sz = sizeof(client_adr);
//	m_clientSocket = accept(m_sock, (sockaddr*)&client_adr, &cli_sz);
//	if (m_clientSocket == INVALID_SOCKET) {
//		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "Accept failed." << std::endl;
//		return false;
//	}
//	return true;
//}

int CClientSocket::DealCommand()
{
	if (m_sock == INVALID_SOCKET) {
		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "invalid client socket" << std::endl;
		return -1;
	}
	char* buffer = m_buffer.data();
	// 此处解决粘包问题的算法思路需要注意一下
	// 此处好像解析到一个完整包就返回结果终止了，如果粘包了，第二个包的较前段数据会丢弃？？？
	static size_t index = 0;
	while (true)
	{
		if (index > 0) {	// 有滞留数据，先把收到的数据完全解析完
			size_t bufferLen = index;
			m_packet = Packet((BYTE*)buffer, bufferLen);
			if (bufferLen > 0) {
				memmove(buffer, buffer + bufferLen, BUFFER_SIZE - bufferLen);
				index -= bufferLen;
				return m_packet.sCmd;
			}
		}

		size_t len = recv(m_sock, buffer + index, BUFFER_SIZE - index, 0);
		if (len <= 0) {
			std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "recv error" << std::endl;
			return -1;
		}
		index += len;
		len = index;
		// =运算符重载
		m_packet = Packet((BYTE*)buffer, len);
		if (len >= 0) {
			memmove(buffer, buffer + len, BUFFER_SIZE - len);
			index -= len;
			return m_packet.sCmd;
		}
	}
	return -1;
}

bool CClientSocket::Send(const char* pData, int nSize)
{
	if (m_sock == INVALID_SOCKET) {
		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "invalid client socket" << std::endl;
		return false;
	}
	return send(m_sock, pData, nSize, 0) > 0;
}

bool CClientSocket::Send(Packet& pack)
{
	if (m_sock == -1)
		return false;
	return send(m_sock, pack.Data(), pack.Size(), 0);
}

bool CClientSocket::GetFilePath(std::string& filePath)
{
	if ((m_packet.sCmd >= 2) && (m_packet.sCmd <= 4)) {
		filePath = m_packet.strData;
		return true;
	}
	return false;
}

bool CClientSocket::GetMouseEvent(const MOUSEEV& mouse)
{
	if (m_packet.sCmd == 5) {
		memcpy((void*)&mouse, m_packet.strData.c_str(), sizeof(mouse));
		return true;
	}
	return false;
}

void CClientSocket::CloseSocket()
{
	closesocket(m_sock);
}

CClientSocket::CClientSocket() :m_sock(INVALID_SOCKET)
{
	m_buffer.resize(BUFFER_SIZE);
	memset(m_buffer.data(), 0, BUFFER_SIZE);
	if (InitSocketEnv() == FALSE) {
		MessageBox(NULL, _T("无法初始化套接字环境,请检查网络设置"), _T("初始化错误"), MB_OK | MB_ICONERROR);
		exit(0);
	}
}

BOOL CClientSocket::InitSocketEnv() {
	// 初始化套接字
	WSADATA data;
	if (WSAStartup(MAKEWORD(1, 1), &data) != 0) {
		std::cerr << __FILE__ << __LINE__ << __FUNCTION__ << "WSAStartu fail" << std::endl;
		return FALSE;
	}
	return TRUE;
}

CClientSocket::~CClientSocket()
{
	CloseSocket();
	//WSACleanup();
}
