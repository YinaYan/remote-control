﻿
// RemoteClientDlg.h: 头文件
//

#pragma once
#include "StatusDlg.h"

#define WM_SEND_PACKET (WM_USER + 1)	// 发送数据包的消息ID

// CRemoteClientDlg 对话框
class CRemoteClientDlg : public CDialogEx
{
// 构造
public:
	CRemoteClientDlg(CWnd* pParent = nullptr);	// 标准构造函数
	void SetImageStatus(bool isFull = false) {
		m_isFull = isFull;
	}
	void SetImageData(bool isImageData = false) {
		m_isImageData = isImageData;
	}
	bool isFull() const {
		return m_isFull;
	}
	CImage& GetImage() {
		return m_image;
	}
// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_REMOTECLIENT_DIALOG };
#endif
private:
	CImage m_image;	// 缓存
	bool m_isFull;	// 缓存是否有数据
	bool m_isImageData;	// 是否保持向客户端请求数据
private:
	int SendCommandPacket(int nCmd, bool bAutoClose = true, BYTE* pData = NULL, size_t nLength = 0);
	CString GetPath(HTREEITEM hTree);
	void DeleteTreeChildrenItem(HTREEITEM hTree);
	void LoadFileInfo();
	void LoadCurentFileInfo();
	void ThreadOnDownload();
	void ThreadWatchData();
	static void ThreadEntryForWatchData(void* arg);
	static void ThreadOnDownloadFile(void* arg);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
// 实现
protected:
	HICON m_hIcon;
	CStatusDlg m_dlgStatus;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnTest();
	DWORD m_server_address;
	CString m_nPort;
	afx_msg void OnBnClickedBtnFileinfo();
	CTreeCtrl tree;
	afx_msg void OnNMDblclkTreeDir(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNMClickTreeDir(NMHDR* pNMHDR, LRESULT* pResult);
	// 显示文件
	CListCtrl m_List;
	afx_msg void OnNMRClickList1File(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDownloadFile();
	afx_msg void OnDeleteFile();
	afx_msg void OnRunFile();
	afx_msg LRESULT OnSendPacket(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedEditStartWatch();
};
