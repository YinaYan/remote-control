﻿
// RemoteClientDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "RemoteClient.h"
#include "RemoteClientDlg.h"
#include "afxdialogex.h"
#include "ClientSocket.h"
#include "WatchDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define SERVER_PORT "9567"
#define SERVER_IP	MECHREVO_IP
#define MI_IP	0xC0A80105	// 192.168.1.5
#define MECHREVO_IP  0xC0A80104//192.168.1.4


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CRemoteClientDlg 对话框



CRemoteClientDlg::CRemoteClientDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_REMOTECLIENT_DIALOG, pParent)
	, m_server_address(0)
	, m_nPort(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

int CRemoteClientDlg::SendCommandPacket(int nCmd,bool BAutoClose, BYTE* pData, size_t nLength)
{
	// true 将控件信息更新到变量
	UpdateData();
	CClientSocket* pClient = CClientSocket::GetInstance();
	bool ret = pClient->InitSocket(m_server_address, atoi((LPCTSTR)m_nPort));
	if (!ret) {
		AfxMessageBox("网络初始化失败！");
	}
	Packet pack(nCmd, pData, nLength);
	pClient->Send(pack);
	int cmd = pClient->DealCommand();
	if (cmd != -1) {
		TRACE("recv server cmd:%d\r\n", cmd);
	}
	if (BAutoClose) {
		pClient->CloseSocket();
	}
	return cmd;
}

void CRemoteClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_IPAddress(pDX, IDC_IPADDRESS_SEVE, m_server_address);
	DDX_Text(pDX, IDC_EDIT_PORT, m_nPort);
	DDX_Control(pDX, IDC_TREE_DIR, tree);
	DDX_Control(pDX, IDC_LIST1_FILE, m_List);
}

BEGIN_MESSAGE_MAP(CRemoteClientDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_TEST, &CRemoteClientDlg::OnBnClickedBtnTest)
	ON_BN_CLICKED(IDC_BTN_FILEINFO, &CRemoteClientDlg::OnBnClickedBtnFileinfo)
	ON_NOTIFY(NM_DBLCLK, IDC_TREE_DIR, &CRemoteClientDlg::OnNMDblclkTreeDir)
	ON_NOTIFY(NM_CLICK, IDC_TREE_DIR, &CRemoteClientDlg::OnNMClickTreeDir)
	ON_NOTIFY(NM_RCLICK, IDC_LIST1_FILE, &CRemoteClientDlg::OnNMRClickList1File)
	ON_COMMAND(ID_DOWNLOAD_FILE, &CRemoteClientDlg::OnDownloadFile)
	ON_COMMAND(ID_DELETE_FILE, &CRemoteClientDlg::OnDeleteFile)
	ON_COMMAND(ID_RUN_FILE, &CRemoteClientDlg::OnRunFile)
	ON_MESSAGE(WM_SEND_PACKET,&CRemoteClientDlg::OnSendPacket)
	ON_BN_CLICKED(IDC_EDIT_START_WATCH, &CRemoteClientDlg::OnBnClickedEditStartWatch)
END_MESSAGE_MAP()


// CRemoteClientDlg 消息处理程序

BOOL CRemoteClientDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标
	m_isFull = false;
	// TODO: 在此添加额外的初始化代码
	// 更新控件到变量
	UpdateData();
	m_server_address = SERVER_IP;
	m_nPort = _T(SERVER_PORT);
	// 更新变量到控件
	UpdateData(FALSE);
	m_dlgStatus.Create(IDD_DIALOG_STATUS, this);
	m_dlgStatus.ShowWindow(SW_HIDE);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CRemoteClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CRemoteClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CRemoteClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CRemoteClientDlg::OnBnClickedBtnTest()
{
	SendCommandPacket(309);
}


void CRemoteClientDlg::OnBnClickedBtnFileinfo()
{
	SendCommandPacket(1);
	tree.DeleteAllItems();
	std::string drivers = CClientSocket::GetInstance()->GetPacket().strData;
	std::string dr;

	for (size_t i = 0; i < drivers.size(); i++)
	{
		if (drivers[i] == ',') {
			dr += ":";
			// 向树形控件中添加项目
			HTREEITEM hTemp = tree.InsertItem(dr.c_str(), TVI_ROOT, TVI_LAST);
			tree.InsertItem(NULL, hTemp, TVI_LAST);
			dr.clear();
			continue;
		}
		dr += drivers[i];
	}
	if (dr.size() > 0) {
		dr += ":";
		// 向树形控件中添加项目
		HTREEITEM hTemp = tree.InsertItem(dr.c_str(), TVI_ROOT, TVI_LAST);
		tree.InsertItem(NULL, hTemp, TVI_LAST);
	}
}

CString CRemoteClientDlg::GetPath(HTREEITEM hTree)
{
	CString strRet, strTmp;
	do {
		strTmp = tree.GetItemText(hTree);
		strRet = strTmp + '\\' + strRet;
		hTree = tree.GetParentItem(hTree);
	} while (hTree != NULL);
	return strRet;
}

void CRemoteClientDlg::DeleteTreeChildrenItem(HTREEITEM hTree)
{
	HTREEITEM hSub = NULL;
	do {
		hSub = tree.GetChildItem(hTree);
		if (hSub != NULL) {
			tree.DeleteItem(hSub);
		}
	} while (hSub != NULL);
}

void CRemoteClientDlg::LoadFileInfo()
{
	CPoint ptMouse;
	GetCursorPos(&ptMouse);
	tree.ScreenToClient(&ptMouse);
	HTREEITEM hTreeSelected = tree.HitTest(ptMouse, 0);
	if (hTreeSelected == NULL) {
		return;
	}
	// 点击的目标是文件
	if (tree.GetChildItem(hTreeSelected) == NULL) {
		return;
	}
	DeleteTreeChildrenItem(hTreeSelected);
	m_List.DeleteAllItems();
	CString strPath = GetPath(hTreeSelected);
	int nCmd = SendCommandPacket(2, false, (BYTE*)(LPCTSTR)strPath, strPath.GetLength());
	PFILEINFO pInfo = (PFILEINFO)CClientSocket::GetInstance()->GetPacket().strData.c_str();
	while (pInfo->HasNext)
	{
		TRACE("[%s] isdir %d\r\n", pInfo->szFileName, pInfo->IsDirectory);
		if (pInfo->IsDirectory) {
			if ((CString(pInfo->szFileName) == ".") || (CString(pInfo->szFileName) == "..")) {
				int cmd = CClientSocket::GetInstance()->DealCommand();
				TRACE("ack:%d\r\n", cmd);
				if (cmd < 0) {
					break;
				}
				pInfo = (PFILEINFO)CClientSocket::GetInstance()->GetInstance()->GetPacket().strData.c_str();
				continue;
			}
			// 添加文件节点到ui
			HTREEITEM hTemp = tree.InsertItem(pInfo->szFileName, hTreeSelected, TVI_LAST);
			tree.InsertItem("", hTemp, TVI_LAST);
		}
		else {
			m_List.InsertItem(0, pInfo->szFileName);
		}

		int cmd = CClientSocket::GetInstance()->DealCommand();
		TRACE("ack:%d\r\n", cmd);
		if (cmd < 0) {
			break;
		}
		pInfo = (PFILEINFO)CClientSocket::GetInstance()->GetInstance()->GetPacket().strData.c_str();
	}
	CClientSocket::GetInstance()->CloseSocket();
}

void CRemoteClientDlg::LoadCurentFileInfo()
{
	HTREEITEM hTree = tree.GetSelectedItem();
	CString strPath = GetPath(hTree);
	m_List.DeleteAllItems();
	int nCmd = SendCommandPacket(2, false, (BYTE*)(LPCTSTR)strPath, strPath.GetLength());
	PFILEINFO pInfo = (PFILEINFO)CClientSocket::GetInstance()->GetPacket().strData.c_str();
	while (pInfo->HasNext)
	{
		TRACE("[%s] isdir %d\r\n", pInfo->szFileName, pInfo->IsDirectory);
		if (!pInfo->IsDirectory) {
			m_List.InsertItem(0, pInfo->szFileName);

		}
		int cmd = CClientSocket::GetInstance()->DealCommand();
		TRACE("ack:%d\r\n", cmd);
		if (cmd < 0) {
			break;
		}
		pInfo = (PFILEINFO)CClientSocket::GetInstance()->GetInstance()->GetPacket().strData.c_str();
	}
	CClientSocket::GetInstance()->CloseSocket();
}

void CRemoteClientDlg::OnNMDblclkTreeDir(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
	LoadFileInfo();
}


void CRemoteClientDlg::OnNMClickTreeDir(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
	LoadFileInfo();
}


// 选中右键
void CRemoteClientDlg::OnNMRClickList1File(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
	CPoint ptMouse,ptList;
	GetCursorPos(&ptMouse);
	ptList = ptMouse;
	m_List.ScreenToClient(&ptList);
	int ListSelected = m_List.HitTest(ptList);
	if (ListSelected < 0) {
		return;
	}
	CMenu menu;
	// 加载整个菜单栏
	menu.LoadMenu(IDR_MENU_RCLICK);
	CMenu* pPupup = menu.GetSubMenu(0);
	if (pPupup != NULL) {
		pPupup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptMouse.x, ptMouse.y, this);
	}
}

void CRemoteClientDlg::ThreadOnDownload()
{
	// 获取列表控件 `m_List` 中当前选中的行索引
	int nListSelected = m_List.GetSelectionMark();
	// 根据选中的行索引获取列表控件中第一列（索引为0）的文本内容，即文件名
	CString strFile = m_List.GetItemText(nListSelected, 0);
	// 第一个参数表示创建一个保存文件对话框。
	// 第二个参数表示默认文件扩展名，这里传入 `NULL` 表示没有默认扩展名。
	// 第三个参数表示默认选中的文件名，即之前选中的文件名。
	// 第四个参数是对话框的样式，`OFN_HIDEREADONLY` 表示隐藏只读文件选项，`OFN_OVERWRITEPROMPT` 表示在文件已存在时显示覆盖提示。
	// 第五个参数是对话框的父窗口，这里传入 `this` 表示将当前窗口作为父窗口。
	CFileDialog dlg(FALSE, NULL, strFile, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, NULL, this);
	if (dlg.DoModal() == IDOK) {	// 显示保存文件对话框，并检查用户是否点击了对话框上的“确定”按钮
		// 打开一个文件用于写入
		FILE* pFile = fopen(dlg.GetPathName(), "wb+");
		if (pFile == NULL) {
			AfxMessageBox(_T("本地没有权限保存文件，或者文件无法创建！！！"));
			m_dlgStatus.ShowWindow(SW_HIDE);
			EndWaitCursor();
			return;
		}
		// 获取树形控件 `tree` 中选中的项，并构建完整的文件路径
		HTREEITEM hSelected = tree.GetSelectedItem();
		strFile = GetPath(hSelected) + strFile;
		TRACE("%s\r\n", LPCSTR(strFile));
		// 发送下载文件命令给服务器，并传递文件路径和长度信息
		int ret = SendMessage(WM_SEND_PACKET, 4 << 1 | 0, (LPARAM)(LPCSTR)strFile);
		if (ret < 0) {
			MessageBox(_T("下载失败！"));
			m_dlgStatus.ShowWindow(SW_HIDE);
			EndWaitCursor();
			TRACE("下载失败 ret = %d\r\n", ret);
			return;
		}
		CClientSocket* pClient = CClientSocket::GetInstance();
		do {
			long long nLength = *(long long*)pClient->GetPacket().strData.c_str();
			if (nLength == 0) {
				AfxMessageBox(_T("文件长度为零或者无法读取文件！！！"));
				return;
			}
			long long nCount = 0;
			while (nCount < nLength)
			{	// 在循环中接收文件数据并写入到本地文件中，直到文件数据传输完成
				ret = pClient->DealCommand();
				if (ret < 0) {
					AfxMessageBox(_T("传输失败！！"));
					TRACE("传输失败： ret = %d\r\n", ret);
					return;
				}
				fwrite(pClient->GetPacket().strData.c_str(), 1, pClient->GetPacket().strData.size(), pFile);
				nCount += pClient->GetPacket().strData.size();
			}
		} while (false);
		fclose(pFile);
		pClient->CloseSocket();
		m_dlgStatus.ShowWindow(SW_HIDE);
		EndWaitCursor();
		MessageBox(_T("下载完成！"), _T("完成"));
	}
}

void CRemoteClientDlg::ThreadWatchData()
{
	CClientSocket* pClient = NULL;
	do {
		pClient = CClientSocket::GetInstance();
	} while (pClient == NULL);
	ULONGLONG tick = GetTickCount64();
	//m_hWnd
	while (m_isImageData) {
		int ret = SendMessage(WM_SEND_PACKET, 6 << 1 | 1);
		if (ret == 6) {
				if (!m_isFull) {
					BYTE* pData = (BYTE*)pClient->GetPacket().strData.c_str();
					HGLOBAL hMem = GlobalAlloc(GMEM_MOVEABLE, 0);
					if (hMem == NULL) {
						TRACE("内存不足了！");
						Sleep(1);
						continue;
					}
					IStream* pStream = NULL;
					HRESULT hRet = CreateStreamOnHGlobal(hMem, TRUE, &pStream);
					if (hRet == S_OK) {
						ULONG length = 0;
						pStream->Write(pData, pClient->GetPacket().strData.size(), &length);
						LARGE_INTEGER bg = { 0 };
						pStream->Seek(bg, STREAM_SEEK_SET, NULL);
						if ((HBITMAP)m_image != NULL) {
							m_image.Destroy();
						}
						m_image.Load(pStream);
						m_isFull = true;
					}
				}
				else {
					Sleep(1);
				}
		}
		else {
			// 防止过度占用CPU的极端情况
			Sleep(1);
		}
	}
}

void CRemoteClientDlg::ThreadEntryForWatchData(void* arg)
{
	CRemoteClientDlg* thiz = (CRemoteClientDlg*)arg;
	thiz->ThreadWatchData();
	_endthread();
}

void CRemoteClientDlg::ThreadOnDownloadFile(void* arg)
{
	CRemoteClientDlg* thiz = (CRemoteClientDlg*)arg;
	thiz->ThreadOnDownload();
	_endthread();
}

// 处理用户点击菜单下载文件的操作
void CRemoteClientDlg::OnDownloadFile()
{
	// 大文件传输：开线程+进度条展示
	_beginthread(CRemoteClientDlg::ThreadOnDownloadFile, 0, this);
	BeginWaitCursor();
	m_dlgStatus.m_info.SetWindowText(_T("正在下载..."));
	m_dlgStatus.CenterWindow(this);
	m_dlgStatus.ShowWindow(SW_SHOW);
	m_dlgStatus.SetActiveWindow();
}


void CRemoteClientDlg::OnDeleteFile()
{
	// TODO: 在此添加命令处理程序代码
	// 获取树形控件 `tree` 中选中的项，并构建完整的文件路径
	HTREEITEM hSelected = tree.GetSelectedItem();
	CString strPath = GetPath(hSelected);
	// 获取列表控件 `m_List` 中当前选中的行索引
	int nSelected = m_List.GetSelectionMark();
	CString strFile = m_List.GetItemText(nSelected, 0);
	strFile = strPath + strFile;
	int ret = SendCommandPacket(9, true, (BYTE*)(LPCSTR)strFile, strFile.GetLength());
	if (ret < 0) {
		AfxMessageBox(_T("删除失败！！"));
		TRACE("删除失败： ret = %d\r\n", ret);
		return;
	}
	// 更新文件信息
	LoadCurentFileInfo();
}


void CRemoteClientDlg::OnRunFile()
{
	// TODO: 在此添加命令处理程序代码
	// 获取树形控件 `tree` 中选中的项，并构建完整的文件路径
	HTREEITEM hSelected = tree.GetSelectedItem();
	CString strPath = GetPath(hSelected);
	// 获取列表控件 `m_List` 中当前选中的行索引
	int nSelected = m_List.GetSelectionMark();
	CString strFile = m_List.GetItemText(nSelected, 0);
	strFile = strPath + strFile;
	int ret = SendCommandPacket(3, true, (BYTE*)(LPCSTR)strFile, strFile.GetLength());
	if (ret < 0) {
		AfxMessageBox(_T("打开失败！！"));
		TRACE("打开失败： ret = %d\r\n", ret);
	}
}

LRESULT CRemoteClientDlg::OnSendPacket(WPARAM wParam, LPARAM lParam)
{
	int cmd = wParam >> 1;
	int ret = 0;
	int autoClose = wParam & 1;
	switch (cmd)
	{
	case 4:
	{
		CString strFile = (LPCSTR)lParam;
		ret = SendCommandPacket(cmd, autoClose, (BYTE*)(LPCSTR)strFile, strFile.GetLength());
	}
		break;
	case 5:	// 鼠标操作
	{
		ret = SendCommandPacket(cmd, autoClose, (BYTE*)lParam, sizeof(MOUSEEV));
	}
	break;
	default:
		//ret = -1;
		ret = SendCommandPacket(cmd, autoClose);
		break;
	}
	return ret;
}

// 创建监控窗口
void CRemoteClientDlg::OnBnClickedEditStartWatch()
{
	// TODO: 在此添加控件通知处理程序代码
	// 以模态方式开启新窗口watchDialog(新窗口带有定时器)
	CWatchDialog dlg(this);
	// 开启线程函数，用于获取实时图片数据
	SetImageData(true);
	_beginthread(CRemoteClientDlg::ThreadEntryForWatchData, 0, this);
	dlg.DoModal();
}
