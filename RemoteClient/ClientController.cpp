﻿#include "pch.h"
#include "ClientController.h"

// 声明静态成员变量
std::map<UINT, CClientController::MSGFUNC> CClientController::m_mapFunc;
CClientController::~CClientController()
{
	WaitForSingleObject(m_hThread, 100);
}

unsigned __stdcall CClientController::threadEntry(void* arg)
{
	CClientController* thiz = (CClientController*)arg;
	thiz->threadFunc();
	_endthreadex(0);
	return 0;
}

void CClientController::threadFunc()
{
	MSG msg;
	while (::GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		if (msg.message == WM_SEND_MESSAGE) {
			MSGINFO* pmsg = (MSGINFO*)msg.wParam;
			HANDLE hEvent = (HANDLE)msg.lParam;
			auto it = m_mapFunc.find(msg.message);
			if (it != m_mapFunc.end()) {
				pmsg->result = (this->*it->second)(pmsg->msg.message, pmsg->msg.wParam, pmsg->msg.lParam);
			}
			else {
				pmsg->result = -1;
			}
			SetEvent(hEvent);
		}
		else {
			auto it = m_mapFunc.find(msg.message);
			if (it != m_mapFunc.end()) {
				(this->*it->second)(msg.message, msg.wParam, msg.lParam);
			}
		}
	}
}

LRESULT CClientController::OnShowStatus(UINT nMsg, WPARAM wParam, LPARAM lParam)
{
	return LRESULT();
}

LRESULT CClientController::OnShowWatcher(UINT nMsg, WPARAM wParam, LPARAM lParam)
{
	return LRESULT();
}

CClientController::CClientController() :m_statusDlg(&m_remoteDlg), m_watchDlg(&m_remoteDlg), m_isClosed(true),
m_hThreadWatch(INVALID_HANDLE_VALUE), m_hThread(INVALID_HANDLE_VALUE), m_nThreadID(-1)
{
	struct { UINT nMsg; MSGFUNC func; }MsgFuncs[] = {
		{WM_SHOW_STATUS,&CClientController::OnShowStatus},
		{WM_SHOW_WATCH,&CClientController::OnShowWatcher},
		{(UINT)-1,NULL}
	};
	for (int i = 0; MsgFuncs[i].func != NULL; i++) {
		m_mapFunc.insert(std::pair<UINT, MSGFUNC>(MsgFuncs[i].nMsg, MsgFuncs[i].func));
	}
}

int CClientController::InitController()
{
	m_hThread = (HANDLE)_beginthreadex(
		NULL, 0,
		&CClientController::threadEntry,
		this, 0, &m_nThreadID);//CreateThread

	m_statusDlg.Create(IDD_DIALOG_STATUS, &m_remoteDlg);
	return 0;
}

int CClientController::Invoke(CWnd*& pMainWnd)
{
	pMainWnd = &m_remoteDlg;
	return m_remoteDlg.DoModal();
}

